<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\VenueController;

Route::get('/venues', [VenueController::class, 'index']);

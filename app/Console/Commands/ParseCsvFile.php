<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ParseCsvFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:parse-csv-file {file : The path to the CSV file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert CSV file to JSON and XML';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $file = $this->argument('file');
        $outputDirectory = 'storage/converted/';
        if (!file_exists($outputDirectory)) {
            mkdir($outputDirectory, 0777, true);
        }

        $jsonFilePath = $outputDirectory . 'venues.json';
        $xmlFilePath = $outputDirectory . 'venues.xml';

        $jsonFile = fopen($jsonFilePath, 'w');
        $xmlFile = fopen($xmlFilePath, 'w');

        $csvFile = fopen($file, 'r');
        $headers = fgetcsv($csvFile);
        fwrite($jsonFile, '[');

        $xml = new \SimpleXMLElement("<?xml version=\"1.0\"?><venues></venues>");

        while (($row = fgetcsv($csvFile)) !== false) {
            $venue = [
                'id' => $row[0],
                'name' => $row[1],
                'image' => $row[2],
                'discount_percentage' => (float) $row[3]
            ];

            fwrite($jsonFile, json_encode($venue, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
            fwrite($jsonFile, ",");

            // Write to XML file
            $xmlVenue = $xml->addChild('venue');
            foreach ($venue as $key => $value) {
                $xmlVenue->addChild($key, htmlspecialchars($value));
            }
        }

        fseek($jsonFile, -1, SEEK_END);
        fwrite($jsonFile, ']');
        fclose($jsonFile);
        file_put_contents($xmlFilePath, $xml->asXML());
        fclose($csvFile);
        $this->info('Conversion completed successfully.');
    }
}

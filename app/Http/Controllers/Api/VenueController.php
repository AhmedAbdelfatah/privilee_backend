<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\VenueFilterRequest;
use Illuminate\Http\JsonResponse;
use App\Http\Utilities\ApiResponse;

class VenueController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(VenueFilterRequest $request):JsonResponse
    {

        $venues = json_decode(file_get_contents(storage_path('converted/venues.json')), true);

        if ($request->has('name')) {
            $name = $request->input('name');
            $venues = array_filter($venues, function ($venue) use ($name) {
                return stripos($venue['name'], $name) !== false;
            });
        }

        if ($request->has('discount_percentage')) {
            $discountPercentage = (int) $request->input('discount_percentage');
            $venues = array_filter($venues, function ($venue) use ($discountPercentage) {
                return $venue['discount_percentage'] >= $discountPercentage;
            });
        }

        $perPage = $request->input('per_page', 10);
        $currentPage = $request->input('page', 1);
        $venues = array_slice($venues, ($currentPage - 1) * $perPage, $perPage);

        return ApiResponse::success($venues, 'Request successful');

    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class VenueFilterRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'string|min:3|max:255',
            'discount_percentage' => 'numeric|min:0|max:100',
        ];
    }
    public function messages()
    {
        return [
            'discount_percentage.numeric' => 'The discount_percentage field should be a numeric value',
            'discount_percentage.min' => 'The discount_percentage field should be at least :min',
            'discount_percentage.max' => 'The discount_percentage field should be at most :max',
            'name.string' => 'The name field should be a string.',
            'name.min' => 'The name field should be at least :min characters',
            'name.max' => 'The name field should be at most :max characters',
        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success'   => false,
            'message'   => 'Validation errors',
            'data'      => $validator->errors()
        ]));
    }
    public function filters()
    {
        return [
            'discount_percentage' => 'trim',
            'name' => 'trim|escape'
        ];
    }
}

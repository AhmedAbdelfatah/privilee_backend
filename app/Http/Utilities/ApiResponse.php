<?php

namespace App\Http\Utilities;

use Illuminate\Http\JsonResponse;

class ApiResponse
{
    public static function success(array $data, string $message = '', int $statusCode = 200): JsonResponse
    {
        return response()->json([
            'success' => true,
            'message' => $message,
            'data' => $data
        ], $statusCode);
    }
}
